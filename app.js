/**
 * Created by reddy on 6/30/2017.
 */
var app = angular.module('myApp',[]);
app.controller('myController',function($scope, $http) {
    $scope.searchStr;
    $scope.country = 'us'
    $scope.entity = 'software';
    $scope.str = "https://itunes.apple.com/search?term=google&country=us&entity=software&limit=12";
    $http.get($scope.str).then(function (response) {
        $scope.results = response.data.results;
    });
    $scope.getData = function (){
        $scope.str = "https://itunes.apple.com/search?term="+$scope.searchStr+"&country="+$scope.country+"&entity=software&limit=32";
        $http.get($scope.str).then(function (response) {
        $scope.results = response.data.results;
        $scope.searchStr = "";
    });
    }
});
